function(BISON_FIXUP_write)

    set(arguments ${ARGN})
    list(SUBLIST arguments 1 -1 keywords)
    cmake_parse_arguments(
            BISON_FIXUP_write
            IO_REDIRECT
            "YY;OUTPUT"
            TARGETS
            ${keywords}
    )

    set(replace_script ${CMAKE_CURRENT_BINARY_DIR}/${ARGV0}-replace.cmake)


    if(BISON_FIXUP_write_IO_REDIRECT)
        set(io_redirect
                "string(REPLACE \"fprintf\" \"sfprintf\" file_contents \"\${file_contents}\")\n"
                "string(REPLACE \"FILE\" \"Sfio_t\" file_contents \"\${file_contents}\")\n"
                "string(REPLACE \"stderr\" \"sfstderr\" file_contents \"\${file_contents}\")\n")
    else()
        set(io_redirect "")
    endif()

    string(TOUPPER ${BISON_FIXUP_write_YY} YY_UPPER)
    FILE(WRITE ${replace_script})

    foreach(target_file ${BISON_FIXUP_write_TARGETS})
        FILE(
                APPEND ${replace_script}
                "file(READ \"${target_file}\" file_contents)\n"
                "string(REPLACE \"yy\" \"${BISON_FIXUP_write_YY}\" file_contents \"\${file_contents}\")\n"
                "string(REPLACE \"YY\" \"${YY_UPPER}\" file_contents \"\${file_contents}\")\n"
                "string(REPLACE \"unsigned long int\" \"uint_64_t\" file_contents \"\${file_contents}\")\n"
                "string(REPLACE \"unsigned long\" \"uint_64_t\" file_contents \"\${file_contents}\")\n"
                "${io_redirect}"
                "file(WRITE \"${target_file}\" \"\${file_contents}\")\n"
        )
    endforeach()
    set(${BISON_FIXUP_write_OUTPUT} ${replace_script} PARENT_SCOPE)
endfunction()


macro(BISON_YYREPLACE)

    set(arguments ${ARGN})
    list(SUBLIST arguments 1 -1 keywords)
    cmake_parse_arguments(
            BISON_YYREPLACE
            ""
            "YY;DEPENDS;BASENAME"
            ""
            ${keywords}
    )

    set(target_name ${ARGV0})
    if(BISON_YYREPLACE_YY EQUAL "")
        set(BISON_YYREPLACE_YY ${target_name})
    endif()

    if(BISON_YYREPLACE_BASENAME EQUAL "")
        set(BISON_YYREPLACE_BASENAME ${target_name})
    endif()

    set(target_base ${CMAKE_CURRENT_BINARY_DIR}/${BISON_YYREPLACE_BASENAME})
    set(targets ${target_base}.c ${target_base}.h)

    BISON_TARGET(
            ${target_name}
            ${BISON_YYREPLACE_DEPENDS}
            ${target_base}.c
            DEFINES_FILE ${target_base}.h
    )

    add_custom_target(
            ${target_name}
            DEPENDS ${targets})

    BISON_FIXUP_write(
            ${target_name}
            OUTPUT BISON_YYREPLACE_OUTPUT
            YY ${BISON_YYREPLACE_YY}
            TARGETS ${targets}
    )

    add_custom_command(
            TARGET ${target_name} POST_BUILD
            BYPRODUCTS ${targets}
            COMMAND cmake -P ${BISON_YYREPLACE_OUTPUT}
    )

    set(BISON_YYREPLACE_${target_name}_OUTPUT ${targets})
endmacro()