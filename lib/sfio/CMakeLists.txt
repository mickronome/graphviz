

file(MAKE_DIRECTORY ${TOP_BINARY_DIR}/FEATURE/)

add_custom_command(
        OUTPUT ${TOP_BINARY_DIR}/FEATURE/sfio
        COMMAND ${TOP_SOURCE_DIR}/iffe - set cc \${CC} \${CCMODE}
                \${CXFLAGS} : run ${CMAKE_CURRENT_SOURCE_DIR}/features/sfio > ${TOP_BINARY_DIR}/FEATURE/sfio
)

include_directories(
        ${TOP_BINARY_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/SFio_dc
)

add_library(
        sfio STATIC
        Sfio_dc/sfdcdos.c
        ${TOP_BINARY_DIR}/FEATURE/sfio
        Sfio_dc/sfdcfilter.c
        Sfio_dc/sfdclzw.c
        Sfio_dc/sfdcseekable.c
        Sfio_dc/sfdcslow.c
        Sfio_dc/sfdcsubstream.c
        Sfio_dc/sfdctee.c
        Sfio_dc/sfdcunion.c
        Sfio_f/_sfclrerr.c
        Sfio_f/_sfdlen.c
        Sfio_f/_sfeof.c
        Sfio_f/_sferror.c
        Sfio_f/_sffileno.c
        Sfio_f/_sfgetc.c
        Sfio_f/_sfllen.c
        Sfio_f/_sfputc.c
        Sfio_f/_sfputd.c
        Sfio_f/_sfputl.c
        Sfio_f/_sfputm.c
        Sfio_f/_sfputu.c
        Sfio_f/_sfslen.c
        Sfio_f/_sfstacked.c
        Sfio_f/_sfulen.c
        Sfio_f/_sfvalue.c
        sfclose.c
        sfclrlock.c
        sfcvt.c
        sfdisc.c
        sfdlen.c
        sfexcept.c
        sfexit.c
        sfextern.c
        sffcvt.c
        sffilbuf.c
        sfflsbuf.c
        sfgetd.c
        sfgetl.c
        sfgetm.c
        sfgetr.c
        sfgetu.c
        sfllen.c
        sfmode.c
        sfmove.c
        sfmutex.c
        sfnew.c
        sfnotify.c
        sfnputc.c
        sfopen.c
        sfpkrd.c
        sfpoll.c
        sfpool.c
        sfpopen.c
        sfprintf.c
        sfprints.c
        sfpurge.c
        sfputd.c
        sfputl.c
        sfputm.c
        sfputr.c
        sfputu.c
        sfraise.c
        sfrd.c
        sfread.c
        sfreserve.c
        sfresize.c
        sfscanf.c
        sfseek.c
        sfset.c
        sfsetbuf.c
        sfsetfd.c
        sfsize.c
        sfsk.c
        sfstack.c
        sfstrtod.c
        sfswap.c
        sfsync.c
        sftable.c
        sftell.c
        sftmp.c
        sfungetc.c
        sfvprintf.c
        sfvscanf.c
        sfwr.c
        sfwrite.c
)
