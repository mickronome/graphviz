#!/usr/bin/env ksh
# exop.h: exparse.h

{
  print "static const char* exop[] = {"
  print "	\"MINTOKEN\","
  sed -e '1,/^[   ]*#[    ]*define[   ][  ]*MINTOKEN/d' \
      -e '/MAXTOKEN/,$d' -e '/^[ 	]*#[ 	]*define[ 	][ 	]*[A-Z]/!d' \
      -e 's/^[ 	]*#[ 	]*define[ 	]*\([A-Z0-9_]*\).*/	"\1",/' < "$1"
  print "};"
}
