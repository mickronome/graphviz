include_directories(
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${GRAPHVIZ_LIB_DIR}/cdt
        ${GRAPHVIZ_LIB_DIR}/cgraph
        ${GRAPHVIZ_LIB_DIR}/gvpr
        ${GRAPHVIZ_LIB_DIR}/common
        ${GRAPHVIZ_LIB_DIR}/gvc
        ${GRAPHVIZ_LIB_DIR}/pathplan
)

add_executable(
        gvpr
        # Source files
        gvprmain.c
)

target_link_libraries(
        gvpr

        libgvpr
        expr
        sfio
        vmalloc
        ingraphs
        ast
        cgraph
        cdt
)

# Installation location of executables
install(
        TARGETS gvpr
        RUNTIME DESTINATION ${BINARY_INSTALL_DIR}
        LIBRARY DESTINATION ${LIBRARY_INSTALL_DIR}
        ARCHIVE DESTINATION ${LIBRARY_INSTALL_DIR}
)

# Specify man pages to be installed
install(
        FILES gvpr.1
        DESTINATION ${MAN_INSTALL_DIR}
)